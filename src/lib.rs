
extern crate tdms;

#[derive(Debug)]
pub struct FilterMods{
  pub order: u32,
  pub cutoff_low: f64,
}

#[derive(Debug)]
pub struct Signal{
  pub data: Vec<f64>,
  pub state: String,
  pub inv_state_exp: String,
  pub ws: String,
}
// todo!("add this test data agnostic");
#[cfg(test)]
mod tests{
  use itertools::Itertools;
  use itertools_num::linspace;

  #[test]
  fn check_signal_const(){
    let sample = linspace(0.,
                          1_000.0,
                          2_000).map_into::<f64>().collect_vec();

    let signal = crate::Signal{
      data: sample,
      state: "0".to_string(),
      inv_state_exp: "compressed air".to_string(),
      ws: "0".to_string(),};

    assert_eq!(signal.inv_state_exp, "compressed air");
    assert_eq!(signal.ws, "0");
    assert_eq!(signal.data.first(),
               Some(&0.));
  }

  #[test]
  #[should_panic]
  fn what_if_i_want_to_panic_saccesfully (){
    let signal = crate::Signal {
      data: vec![1., 2., 3., 0.3, 0.4],
      state: "0".to_string(),
      inv_state_exp: "im nothing".to_string(),
      ws: "0".to_string()};
    assert_eq!(signal.inv_state_exp, "compressed air");
    assert_eq!(signal.ws, "15");
    assert_eq!(signal.data.first(),
               Some(&2.));
  }


  #[test]
  fn check_filter_const(){
    let me_butter = crate::FilterMods{
      order: 2,
      cutoff_low: 200.};
    assert_eq!(me_butter.order, 2);
    assert_eq!(me_butter.cutoff_low, 200.)
  }

  #[ignore = "For later"]
  #[test]
  fn check_the_time_ops()  {
    todo!("Sould test the filtering opperation")
  }
}
