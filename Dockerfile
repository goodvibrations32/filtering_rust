FROM rustdocker/rust:nightly as cargo-build
RUN apt-get update
RUN apt-get install musl-tools -y
RUN /root/.cargo/bin/rustup target add x86_64-unknown-linux-musl
RUN USER=root /root/.cargo/bin/cargo new --bin material
WORKDIR /material
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
RUN RUSTFLAGS=-Clinker=musl-gcc /root/.cargo/bin/cargo build --release --target=x86_64-unknown-linux-musl --features vendored
RUN rm -f target/x86_64-unknown-linux-musl/release/deps/material*
RUN rm src/*.rs
COPY ./src ./src
RUN RUSTFLAGS=-Clinker=musl-gcc /root/.cargo/bin/cargo build --release --target=x86_64-unknown-linux-musl --features vendored
FROM alpine:latest
COPY --from=cargo-build /auth/target/x86_64-unknown-linux-musl/release/material .
CMD ["./material"]
# FROM rustlang/rust:nightly-slim as build

# RUN USER=root cargo new --bin rusty_spectrum
# WORKDIR /rusty_spectrum

# COPY ./Cargo.lock ./Cargo.lock
# COPY ./Cargo.toml ./Cargo.toml

# RUN cargo build --release
# RUN rm src/*.rs

# COPY ./src ./src

# # 5. Build for release.
# # RUN rm ./target/release/deps/rusty_spectrum*
# RUN cargo install --path .

# CMD ["rusty_spectrum"]
